# Safe Fetch

This module wraps `fetch` with a validation layer that prevents fetching non-HTTPS URIs. This makes it a more secure option when accepting user input to the Fetch API.

## Features

- Only allows fetching `https` URLs.
- Only allows fetching valid ICANN domains.
- Prohibits fetching `file://` URIs, `blob://` URIs, IP addresses, non-default ports, internal TLDs (such as `.local` and `.internal`), and anything that isn't a valid ICANN https URL.

## Motivation

The `fetch` implementation in Deno allows [fetching `file://` URIs](https://github.com/denoland/deno/issues/11925#issuecomment-1678084346) by default, which can create a tricky situation when accepting user input to `fetch`. Attacks that involve [pointing public DNS records to localhost](https://gist.github.com/tinogomes/c425aa2a56d289f16a1f4fcb8a65ea65) can also be exploited over HTTP, which is why this module only allows fetching over HTTPS.

## Installation

This module is available on [jsr](https://jsr.io/@soapbox/safe-fetch).

```sh
deno add jsr:@soapbox/safe-fetch
```

## Usage

```ts
import { safeFetch } from '@soapbox/safe-fetch';

// Use it normally:
const response = await safeFetch('https://example.com');

// This throws an error:
const file = await safeFetch('file:///etc/passwd');
```

## Replace the global `fetch`

If you want to replace the global `fetch` with `safeFetch`, you can do so by importing the `load.ts` file:

```ts
import '@soapbox/safe-fetch/load';
```

## License

This is free and unencumbered software released into the public domain.
