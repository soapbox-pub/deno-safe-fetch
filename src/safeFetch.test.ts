import { assertRejects } from '@std/assert';

import { safeFetch } from './safeFetch.ts';

Deno.test('fetching a secure page works', async () => {
  await (await safeFetch('https://soapbox.pub')).text();
});

Deno.test('fetching an http page rejects', async () => {
  await assertRejects(() => safeFetch('http://soapbox.pub'), Error, 'unsupported protocol');
});

Deno.test('fetching a file URI rejects', async () => {
  await assertRejects(() => safeFetch('file:///etc/passwd'), Error, 'unsupported protocol');
});

Deno.test('fetching a data URI rejects', async () => {
  await assertRejects(() => safeFetch('data:text/plain,Hello%2C%20World!'), Error, 'unsupported protocol');
});

Deno.test('fetching a blob URI rejects', async () => {
  await assertRejects(() => safeFetch('blob:https://soapbox.pub/'), Error, 'unsupported protocol');
});

Deno.test('fetching a private TLD rejects', async () => {
  await assertRejects(() => safeFetch('https://soapbox.lan'), Error, 'unsupported host');
  await assertRejects(() => safeFetch('https://soapbox.local'), Error, 'unsupported host');
  await assertRejects(() => safeFetch('https://soapbox.internal'), Error, 'unsupported host');
});

Deno.test('fetching with a port rejects', async () => {
  await assertRejects(() => safeFetch('https://soapbox.pub:80'), Error, 'unsupported port');
  await assertRejects(() => safeFetch('https://soapbox.pub:8080'), Error, 'unsupported port');
});
