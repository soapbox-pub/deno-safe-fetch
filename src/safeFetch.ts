import tldts from 'tldts';

const _fetch = globalThis.fetch;

/**
 * Wraps `fetch` with a validation layer that parses the URL and allows fetching only ICANN domains over HTTPS.
 *
 * See: https://github.com/denoland/deno/issues/11925#issuecomment-1678084346
 */
export const safeFetch: typeof globalThis.fetch = async (
  input: URL | Request | string,
  init?: RequestInit,
): Promise<Response> => {
  const { protocol, port, host, href } = input instanceof Request ? new URL(input.url) : new URL(input);

  if (protocol !== 'https:') {
    throw new Error(`safeFetch: unsupported protocol: ${protocol}`);
  }

  if (port) {
    throw new Error(`safeFetch: unsupported port: ${port}`);
  }

  const { isIcann, isIp, isPrivate } = tldts.parse(href, { mixedInputs: false });

  if (!isIcann || isIp || isPrivate) {
    throw new Error(`safeFetch: unsupported host: ${host}`);
  }

  return await _fetch(input, init);
};
